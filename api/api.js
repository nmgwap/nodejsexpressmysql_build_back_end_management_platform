// 导入express模块
var express = require('express');
// 导入路由模块
var router = express.Router();
// 导入url模块
var url = require('url');
// 导入util(实用工具)模块
var util = require('util');
// 导入querystring模块
var querystring = require('querystring');
// 导入mysql模块 
var mysql = require('mysql');
// 导入数据库配置信息
var dbconfig = require('../db/DBConfig');
// 导入SQL查询语句
var userSQL = require('../db/Usersql');
// 使用DBConfig.js的配置信息创建一个MySQL连接池
var pool = mysql.createPool(dbconfig.mysql);

/* GET users listing. */
/* 登录接口 */
router.get('/login', function(req, res, next) {
    // 获取url中参数集合
    var params = url.parse(req.url, true).query;
    // sql查询语句
    var userSQL = "select * from user where user = " + params.user;
    var SQL = "select * from user where user = " + params.user + " and pwd = " + params.pwd + " limit 1";
    // 启用连接池查询
    pool.getConnection(function(err, connection) {
        // 查询用户名是否存在
        connection.query(userSQL, function(err, results) {
            // 查询结果
            if (results != [] && results != "") {
                // 查询用户名密码是否正确
                connection.query(SQL, function(err, result) {
                    // 查询结果
                    if (result == undefined || result == "undefined") {
                        res.send({ "success": false, "data": {}, "msg": "接口地址错误" });
                    }
                    if (result.length == 1) {
                        res.send({ "success": true, "data": {}, "msg": "登录成功" });
                    } else {
                        res.send({ "success": false, "data": {}, "msg": "用户名或密码错误" });
                    }

                });
                connection.release();
            } else {
                res.send({ "success": false, "data": {}, "msg": "用户名不存在" });
            }

        });
    })
});
/* 注册用户 */
router.post('/registered', function(req, res, next) {
    // 获取请求字段
    var user = req.body.user;
    var pwd = req.body.pwd;
    // sql查询语句
    var usersql = "select * from user where user = '" + user + "'";
    var regsql = "INSERT INTO user (user,pwd, time) VALUES ('" + user + "','" + pwd + "','" + CurentTime() + "')";
    // 启用连接池查询
    pool.getConnection(function(err, connection) {
        // 查询用户名是否存在
        connection.query(usersql, function(err, results) {
            if (results == "") {
                // 插入用户名密码
                connection.query(regsql, function(err, result) {
                    if (result != [] && result != "") {
                        res.send({ "success": true, "data": {}, "msg": "注册成功" });
                    }
                });
                connection.release();
            } else {
                res.send({ "success": false, "data": {}, "msg": "用户名已经存在" });
            }
        });
    });
});

// 获取当前时间方法
function CurentTime() {
    var now = new Date();
    var year = now.getFullYear(); //年
    var month = now.getMonth() + 1; //月
    var day = now.getDate(); //日
    var hh = now.getHours(); //时
    var mm = now.getMinutes(); //分
    var clock = year + "-";
    if (month < 10)
        clock += "0";
    clock += month + "-";
    if (day < 10)
        clock += "0";
    clock += day + " ";
    if (hh < 10)
        clock += "0";
    clock += hh + ":";
    if (mm < 10) clock += '0';
    clock += mm;
    return (clock);
}
module.exports = router;