# nodejs-express-mysql后端接口
> node实现登录/注册接口


#### 说明

>  本项目主要用于熟悉如何用 node写后端接口（post方式和get方式）

>  如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！ ^_^

>  或者您可以 "follow" 一下，我会不断开源更多的有趣的项目

>  开发环境 w7  Chrome 61

>  如有问题请直接在 Issues 中提，或者您发现问题并有非常好的解决方案，欢迎 PR 👍

#### 平台功能说明
------------------------
> 1. 登录页面，数据库获取登录。
> 2. 首页，左边竖向菜单。
> 3. table页面，分页。
> 4. 增加编辑页面，表单验证
> 5. 权限页面（文件树）--ztree插件，后期美化
> 6. 404错误页面
> 7. 登录注册接口，登录接口方式为get，注册为post，并增加接口判断

#### 功能
- [x] 登录 -- 完成
- [x] 注册 -- 完成
- [x] 验证 -- 完成
- [x] 路由跳转 -- 完成
- [x] 表单页面 -- 完成
- [x] 文件树 -- 完成
- [x] error -- 完成

#### 系统部分页面演示
------------------------

注册页
![主页](./zs/7.png "主页")

登录失败接口返回错误信息
![table页面](./zs/8.png "table页面")

#### 目录结构说明
------------------------

```
├── /api/               # 存放接口文件
├── /bin/               # 存放启动项目的脚本文件，默认www。
├── /db/:               # 数据库配置信息
├── /node_modules/      # 存放所有的项目依赖库，默认body-parser、cookie-parser、debug、jade、express、morgan、serve-favicon。
├── /public/            # 静态资源文件夹，默认images、javascripts、stylesheets。
├── /routes/            # 路由文件相当于MVC中的Controller，。
├── /views/             # 页面文件，相当于MVC中的view，Ejs模板或者jade模板，现在已经改为html。
├── package.json        # 项目依赖配置及开发者信息。
├── app.js              # 应用核心配置文件，项目入口，程序从这里开始。相当于php项目中的 index.php、index.html
├── /zs/                # 展示图片
```

#### 前期准备
------------------------
> 1. 本机安装node环境
> 2. 本机安装mysql
> 3. 建user表，或者修改项目数据库连接



#### 运行项目
------------------------
``` 

    安装依赖
	npm install

    运行
    npm start

```

#### 项目源码地址：
------------------------
码云地址：https://gitee.com/ldhblog/bg_mgs_template.git
