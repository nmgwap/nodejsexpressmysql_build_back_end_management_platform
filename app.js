// 导入模块
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
// 导入页面路由
var loginRouter = require('./routes/login');
var indexRouter = require('./routes/index');
var fileTreeRouter = require('./routes/fileTree');
var increaseOrEditorRouter = require('./routes/increaseOrEditor');
var queryTableRouter = require('./routes/queryTable');
var errorRouter = require('./routes/error');
// 导入接口api
var api = require('./api/api');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
// 注释掉ejs
//app.set('view engine', 'ejs');
// 模版引擎替换成html
var ejs = require('ejs');
app.engine('html', ejs.__express);
app.set('view engine', 'html');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// 设置页面路由
app.use('/', loginRouter);
app.use('/login', loginRouter);
app.use('/index', indexRouter);
app.use('/fileTree', fileTreeRouter);
app.use('/increaseOrEditor', increaseOrEditorRouter);
app.use('/queryTable', queryTableRouter);
app.use('/error', errorRouter);

// 设置接口api
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;