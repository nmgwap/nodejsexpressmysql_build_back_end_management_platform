/**
 * 表单验证 
 */
$(".btn").on("click", function() {
    var user = $(".user").val();
    if (user == "") {
        $(".t-box").removeClass("dp-n");
        $(".t-color").html("用户名不能为空！");
        return;
    };
    var pwd = $(".pwd").val();
    if (pwd == "") {
        $(".t-box").removeClass("dp-n");
        $(".t-color").html("密码不能为空！");
        return;
    };
    $.ajax({
        url: "/api/login?user='" + user + "'&pwd='" + pwd + "'",
        success: function(result) {
            console.log(result)
            if (result.success) {
                window.location.href = "index";
                // 本地缓存
                localStorage.setItem("loginType", "1");
                localStorage.setItem("userNmae", user);
            } else {
                $(".t-box").removeClass("dp-n");
                $(".t-color").html(result.msg);

            }
        }
    });
});
//点击去注册按钮
$(".reg").on("click", function() {
    $(".regpwd").removeClass("dp-n");
    $(".regbtn").removeClass("dp-n");
    $(".gologin").removeClass("dp-n");
    $(".btn").addClass("dp-n");
    $(".reg").addClass("dp-n");
});
//点击去登录按钮
$(".gologin").on("click", function() {
    $(".regpwd").addClass("dp-n");
    $(".regbtn").addClass("dp-n");
    $(".gologin").addClass("dp-n");
    $(".btn").removeClass("dp-n");
    $(".reg").removeClass("dp-n");
});
// 注册功能
$(".regbtn").on("click", function() {
    var user = $(".user").val();
    if (user == "") {
        $(".t-box").removeClass("dp-n");
        $(".t-color").html("请输入用户名！");
        return;
    };
    var pwd = $(".pwd").val();
    if (pwd == "") {
        $(".t-box").removeClass("dp-n");
        $(".t-color").html("请输入密码！");
        return;
    };
    var regpwd = $(".regpwd").val();
    if (regpwd == "") {
        $(".t-box").removeClass("dp-n");
        $(".t-color").html("请确认密码！");
        return;
    };
    if (pwd != regpwd) {
        $(".t-box").removeClass("dp-n");
        $(".t-color").html("两次密码不一样！");
        return;
    }
    $.ajax({
        url: "/api/registered",
        data: {
            user: user,
            pwd: pwd,
            regpwd: regpwd
        },
        type: "post",
        dataType: 'json',
        success: function(result) {
            console.log(result)
            if (result.success) {
                window.location.href = "index";
                // 本地缓存
                localStorage.setItem("loginType", "1");
                // 缓存用户名
                localStorage.setItem("userNmae", user);
            } else {
                $(".t-box").removeClass("dp-n");
                $(".t-color").html(result.msg);

            }
        }
    });
});